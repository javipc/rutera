# rutera

### [10.0]
Modificado: Pantalla principal.
Modificado: Botones de grabar y detener.

### [9.1]
Agregado: Botón de detener en la pantalla principal.
Modificado: Menú iniciar y detener en menú fue quitado.
Modificado: Versión del mapa osmdroid.

### [9]
Agregado: Fotografía de perfil para capturas de pantalla.
Modificado: Localizador.
Modificado: Eventos.

### [8 a 4]
Modificado: Eventos.
Agregado: pantalla de ayuda.
y un montón de cosas.

### [5]
Modificado: android 29
Corregido: Error en la base de datos del clima (característica aún no implementada).

### [3]
Agregado: Borrado de sesiones.

### [2]
Mejora: Mapa de velocidad.
Agregado: Duración.
Agregado: Distancia.
Modificado: Apariencia de registros.

### [1]
Versión Inicial.
