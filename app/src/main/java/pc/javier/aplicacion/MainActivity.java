package pc.javier.aplicacion;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import aplicacion.actividad.ActividadAyuda;
import aplicacion.actividad.ActividadSesiones;
import aplicacion.control.ControlPantallaPanel;
import aplicacion.control.ControlPantallaPrincipal;
import pc.javier.menulateralcompatible.R;
import utilidades.basico.EnlaceExterno;
import utilidades.basico.MensajeRegistro;
import utilidades.imagen.AdaptadorImagen;
import utilidades.menu.MenuLateral;
import utilidades.menu.PantallaPrincipalConMenuLateral;


public class MainActivity extends PantallaPrincipalConMenuLateral {


    private ControlPantallaPrincipal control;
    private ControlPantallaPanel panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.principal_pantalla);



        menuLateral = new MenuLateral(this, R.id.panel_layout, R.id.panel_view);

        control = new ControlPantallaPrincipal(this, menuLateral);
        panel = new ControlPantallaPanel(this);

    }

    @Override
    public void onResume () {
        super.onResume();
        //control.habilitar_opciones_inicio();
        invalidateOptionsMenu();

        panel.iniciar();
        control.habilitar_opciones_grabacion ();

    }

    @Override
    public void onDestroy () {
        super.onDestroy();
        panel.detener_localizador();

    }


    @Override
    public void onPause () {
        super.onPause ();
        panel.detener_localizador();

    }





    public void clic_grabar (View v) {
        control.grabar();
        panel.iniciar_todo();
    }


    public void clic_detener (View v) {
        control.detener();
        panel.detener_reloj();
    }




    // Menú lateral -----------------------------------------------------------------

    @Override
    public void clicMenu (MenuItem item) {

        super.clicMenu(item);


        switch (item.getItemId()) {

            case R.id.menu_grabar:
                control.grabar ();
                break;


            case R.id.menu_detener:
                control.detener();
                break;

            case R.id.menu_registros:
                control.iniciarActividad(ActividadSesiones.class);
                break;

            case R.id.menu_fotografia:
                control.elegir_fotografia ();
                break;

            case R.id.menu_proyectos:
                abrir_enlace(Aplicacion.telegram);
                break;

            case R.id.menu_donar:
                abrir_enlace(Aplicacion.donacion);
                break;


            case R.id.menu_ayuda:
                control.iniciarActividad(ActividadAyuda.class);
                break;
        }


    }


    public void clicBoton (View v) {
        menuLateral.alternar();
    }


    private void abrir_enlace (String enlace){
         (new EnlaceExterno(this)).abrirEnlace(enlace);
    }



    public void fab (View v) {
        menuLateral.alternar();
    }




    @Override
    public void onActivityResult(int codigo, int resultado, Intent dato) {
        control.onActivityResult(codigo, resultado, dato);
        super.onActivityResult(codigo, resultado, dato);
    }


    private void refrescar_menu () {

    }
}
